import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { QuotesService } from '../../services/quotes';
//import { Quote } from '../../data/quotes.interface';

@IonicPage()
@Component({
  selector: 'page-quotes',
  templateUrl: 'quotes.html',
})
export class QuotesPage {
  quotes: any[]

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public alertCtrl: AlertController, 
    public qService: QuotesService) {}

  ionViewDidEnter() {
    this.quotes = this.navParams.data
  }

  onAddToFavorites(q: any) {
    const alert = this.alertCtrl.create({
      title: "Add Quote to Favorite",
      subTitle: "Are you sure you want to add this quote?",
      buttons: [
        {
          text: "OK",
          handler: () => {
            this.qService.addToFavoriteQuotes(q)
          }
        },
        {
          text: "Cancel",
          handler: () => {
            console.log("cancelled")
          },
          role: "cancel"
        }
      ]
    })
    alert.present()
  }

  onRemoveFromFavorites(q: any) {
    const alert = this.alertCtrl.create({
      title: "Remove Quote from Favorite",
      subTitle: "Are you sure you want to remove this quote?",
      buttons: [
        {
          text: "OK",
          handler: () => {
            this.qService.removeFromFavoriteQuotes(q)
          }
        },
        {
          text: "Cancel",
          handler: () => {
            console.log("cancelled")
          },
          role: "cancel"
        }
      ]
    })
    alert.present()
  }

  onIsFavorited(q: any) {
    return this.qService.isFavorited(q)
  }

}
