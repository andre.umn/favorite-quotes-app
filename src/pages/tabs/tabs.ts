import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { LibraryPage } from '../library/library';
import { FavoritesPage } from '../favorites/favorites';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  libraryPage = LibraryPage;
  favoritesPage = FavoritesPage;
}
