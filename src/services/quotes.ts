import { Quote } from '../data/quotes.interface';

export class QuotesService {
    favoriteQuotes: Quote[] = []

    addToFavoriteQuotes(q: Quote) {
        this.favoriteQuotes.push(q)
        console.log(this.favoriteQuotes)
    }

    removeFromFavoriteQuotes(q: Quote) {
        let idx = this.favoriteQuotes.findIndex((qElement: Quote) => {
            return q.id == qElement.id
        })
        if(idx!=-1) this.favoriteQuotes.splice(idx, 1)
        console.log(this.favoriteQuotes)
    }

    getAllFavoriteQuote() {
        return this.favoriteQuotes
    }

    isFavorited(q: Quote) {
        let idx = this.favoriteQuotes.findIndex((qElement: Quote) => {
            return q.id == qElement.id
        })
        return idx!=-1? true : false 
    }
}